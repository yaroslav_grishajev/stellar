const path = require('path')
const webpack = require('webpack')
const CommonsChunkPlugin = webpack.optimize.CommonsChunkPlugin
const autoprefixer = require('autoprefixer')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const packageInfo = require('./package.json')

const ENV = packageInfo.config.env = process.env.NODE_ENV || process.env.ENV || 'development'
const isProduction = ENV === 'production'

const envSpecificPlugins = []

if (isProduction) {
  envSpecificPlugins.push(
    new webpack.NoEmitOnErrorsPlugin(),
    new webpack.optimize.UglifyJsPlugin({sourceMap: true, mangle: { keep_fnames: true }}),
    new CopyWebpackPlugin([{
      from: resolve('src/public')
    }])
  )
}

module.exports = {
    entry: {
      'vendor': [
        './src/vendor/vendor.ts',
        './src/vendor/polyfills.ts',
      ],
      'app': './src/boot/index.ts'
    },
    output: {
      path: resolve('dist'),
      publicPath: isProduction ? '/' : 'http://localhost:8080/',
      filename: isProduction ? 'js/[name].[hash].js' : 'js/[name].js',
      chunkFilename: isProduction ? '[id].[hash].chunk.js' : '[id].chunk.js'
    },
    resolve: {
      extensions: ['.ts', '.js', '.json', '.css', '.scss', '.html'],
    },
    devtool: isProduction ? 'source-map' : 'eval-source-map',
    module: {
      rules: [
        {
          test: /\.ts$/,
          loaders: [`awesome-typescript-loader`, 'angular2-template-loader', '@angularclass/hmr-loader'],
          exclude: [/node_modules\/(?!(ng2-.+))/]
        },
        {
          test: /\.(png|jpe?g|gif|svg|woff|woff2|ttf|eot|ico)(\?v=[0-9]\.[0-9]\.[0-9])?$/,
          loader: 'file-loader?name=fonts/[name].[hash].[ext]?'
        },
        {test: /\.json$/, loader: 'json-loader'},
        {
          test: /\.(scss|css)$/,
          loader: ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: 'css-loader!sass-loader'
          }),
          include: [resolve('src'), resolve('src', 'style'), resolve('node_modules')]
        },
        {test: /\.html$/, loader: 'raw-loader',  exclude: resolve('src', 'public')}
      ]
    },
    plugins: [
      new webpack.DefinePlugin({
        APP_CONFIG: JSON.stringify(buildEnvVars()),
        IS_PRODUCTION: isProduction,
      }),
      new webpack.ContextReplacementPlugin(
        /angular(\\|\/)core(\\|\/)@angular/,
        resolve('./src')
      ),
      new webpack.LoaderOptionsPlugin({
        options: {
          tslint: {
            emitErrors: false,
            failOnHint: false
          },
          postcss: [
            autoprefixer({
              browsers: ['last 2 version']
            })
          ]
        },
      }),
      new CommonsChunkPlugin({
        name: ['vendor', 'polyfills']
      }),
      new HtmlWebpackPlugin({
        template: './src/public/index.html',
        chunksSortMode: 'dependency'
      }),
      new ExtractTextPlugin({filename: 'css/[name].[hash].css', disable: !isProduction}),
      ...envSpecificPlugins,
    ],
    devServer: {
      contentBase: './src/public',
      historyApiFallback: true,
      quiet: true,
      stats: 'minimal'
    }
  }

function resolve(...args) {
  return path.join.apply(path, [__dirname, ...args])
}

function buildEnvVars() {
  return Object.keys(packageInfo.config).reduce((config, name) => {
    config[name] = name in process.env ? process.env[name] : packageInfo.config[name]

    return config
  }, {})
}

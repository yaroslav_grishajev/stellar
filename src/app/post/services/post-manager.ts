import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http'
import { ActivatedRoute } from '@angular/router'
import { Observable } from "rxjs";

import { Post } from '../models/post'
import { Config } from '../../../config/env'

@Injectable()
export class PostManager {
  apiUrl: string

  constructor(private http: Http, private config: Config) {
    this.apiUrl = this.config.get('API_URL')
  }

  findAll(): Promise<Post[]> {
    return this.http.get(`${this.apiUrl}posts`)
      .toPromise()
      .then(response => response.json())
  }

  find(id: number): Promise<Post> {
    return this.http.get(`${this.apiUrl}posts/${id}`)
      .toPromise()
      .then(response => response.json())
  }

  findByRoute(activeRoute: ActivatedRoute): Promise<any> {
    return this.resolvePostId(activeRoute)
      .then(id => {
        if (id) {
          return this.find(id)
        }

        return Promise.resolve(null)
      })
  }

  resolvePostId(activeRoute: ActivatedRoute): any {
    return new Promise(resolve => {
      activeRoute.params
        .subscribe(params => resolve(params.id))
    })
  }

  save(post: Post): Promise<Post> {
    const method = post.id ? 'update' : 'create'

    return this[method](post)
      .toPromise()
      .then(response => response.json())
  }

  create(post: Post): Observable<Response> {
    return this.http.post(`${this.apiUrl}posts/`, post)
  }

  update(post: Post): Observable<Response> {
    return this.http.put(`${this.apiUrl}posts/${post.id}`, post)
  }
}
import { Component, OnInit } from '@angular/core'
import { ActivatedRoute } from '@angular/router'

import { PostManager } from '../../services/post-manager'
import { Post } from '../../models/post'

@Component({
  selector: 'post-details',
  templateUrl: './post-details.html',
})
export class PostDetails implements OnInit {
  private post: any = {}

  constructor(private activeRoute: ActivatedRoute, private postManager: PostManager) {}

  ngOnInit() {
    this.fetchPost()
  }

  fetchPost(): Promise<Post> {
    return this.postManager.findByRoute(this.activeRoute)
      .then(post => this.post = post)
  }
}

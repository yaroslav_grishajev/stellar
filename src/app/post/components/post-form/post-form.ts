import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { ActivatedRoute } from '@angular/router'
import { ToasterService } from 'angular2-toaster'

import { PostManager } from '../../services/post-manager'

import './post-form.scss'

@Component({
  selector: 'post-form',
  templateUrl: './post-form.html',
})
export class PostForm implements OnInit {
  private post: any = {}
  private isSubmitted: boolean = false
  private isNew: boolean = true

  constructor(private activeRoute: ActivatedRoute,
              private postManager: PostManager,
              private toast: ToasterService,
              private location: Location) {}

  ngOnInit() {
    this.fetchPost()
  }

  fetchPost(): void {
    this.postManager.findByRoute(this.activeRoute)
      .then(post => {
        if (post) {
          this.isNew = false
          return this.post = post
        }
      })
  }

  tryToSave(form): void {
    this.isSubmitted = true

    if (form.valid) {
      this.saveAndGoBack()
    }
  }

  saveAndGoBack(): void {
    this.postManager.save(this.post)
      .then(post => {
        const action = this.isNew ? 'create' : 'update'
        this.toast.pop('success', 'Success', `Post "${post.title}" ${action}d!`)
        this.location.back()
      })
  }
}

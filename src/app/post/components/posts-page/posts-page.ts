import { Component, OnInit } from '@angular/core'

import { Post } from '../../models/post'
import { PostManager } from '../../services/post-manager'

import './posts-page.scss'

@Component({
  selector: 'posts-page',
  templateUrl: './posts-page.html',
})
export class PostsPage implements OnInit {
  posts: Promise<Post[]>

  constructor(private postManager: PostManager) {}

  ngOnInit() {
    this.posts = this.postManager.findAll()
  }
}

import { RouterModule } from '@angular/router'
import { PostDetails } from '../components/post-details/post-details'
import { PostForm } from '../components/post-form/post-form'

export const ROUTES = RouterModule.forRoot([
  { path: 'create', component: PostForm },
  { path: ':id', component: PostDetails },
  { path: ':id/edit', component: PostForm },
])

import { NgModule } from '@angular/core'
import { CommonModule } from '@angular/common'
import { FormsModule } from '@angular/forms'
import { MaterialModule } from '@angular/material'
import { ToasterModule } from 'angular2-toaster/angular2-toaster'

import { PostsPage } from '../components/posts-page/posts-page'
import { PostDetails } from '../components/post-details/post-details'
import { PostForm } from '../components/post-form/post-form'

import { PostManager } from '../services/post-manager'

import { ROUTES } from './routing'

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    MaterialModule,
    ToasterModule,
    ROUTES
  ],
  exports: [
    PostsPage
  ],
  declarations: [
    PostsPage,
    PostDetails,
    PostForm
  ],
  providers: [
    PostManager
  ],
})
export class PostsModule {
}

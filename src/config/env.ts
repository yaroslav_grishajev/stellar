declare let APP_CONFIG: Object

export class Config {
  get(key: string): any {
    return APP_CONFIG[key]
  }
}

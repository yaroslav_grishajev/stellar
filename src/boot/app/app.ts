import { Component, OnInit } from '@angular/core'
import { Location } from '@angular/common'
import { Router, NavigationEnd } from '@angular/router'

import './app.scss'

@Component({
  selector: 'posts-app',
  templateUrl: './app.html',
})
export class PostsApp implements OnInit {
  private isRoot: boolean = true

  constructor(private router: Router, public location: Location) {}

  ngOnInit() {
    this.watchForRouter()
  }

  watchForRouter(): void {
    this.router.events.subscribe(state => {
      if (state instanceof NavigationEnd) {
        this.isRoot = state.url === '/'
      }
    })
  }
}

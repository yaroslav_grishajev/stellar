import { RouterModule } from '@angular/router'
import { PostsPage } from '../app/post/components/posts-page/posts-page'


export const ROUTES = RouterModule.forRoot([
  { path: '', component: PostsPage }
])

import { NgModule } from '@angular/core'
import { BrowserModule } from '@angular/platform-browser'
import { HttpModule } from '@angular/http'
import { FormsModule } from '@angular/forms'
import { RouterModule }  from '@angular/router';
import { MaterialModule } from '@angular/material'
import { BrowserAnimationsModule } from '@angular/platform-browser/animations'
import { ToasterModule } from 'angular2-toaster/angular2-toaster'

import { PostsModule } from '../app/post/config/module'
import { ROUTES } from './routing'

import { Config } from '../config/env'

import { PostsApp } from './app/app'

@NgModule({
  imports: [
    BrowserModule,
    HttpModule,
    FormsModule,
    RouterModule,
    MaterialModule,
    BrowserAnimationsModule,
    PostsModule,
    ToasterModule,
    ROUTES
  ],
  declarations: [
    PostsApp,
  ],
  providers: [
    Config,
  ],
  bootstrap: [PostsApp]
})
export class AppModule {}
import { enableProdMode } from '@angular/core'
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic'

import { AppModule } from './module'

import '../style/app.scss'

declare const IS_PRODUCTION: boolean

if (IS_PRODUCTION) {
  enableProdMode()
}

platformBrowserDynamic().bootstrapModule(AppModule)

#Posts App

This is a simple app built with Angular which consumes [Sample Rest Api](http://jsonplaceholder.typicode.com/):

##Installation:

1. clone the repository
```git clone git@bitbucket.org:yaroslav_grishajev/stellar.git```

2. install modules
```npm i```

3. start development server
```npm start```

4. or production
```NODE_ENV=production npm start```



##Some Possible improvements

1. Use client side ORM for data handling and API communication

2. Add tests 

3. More thorough code review

4. More features (?)
